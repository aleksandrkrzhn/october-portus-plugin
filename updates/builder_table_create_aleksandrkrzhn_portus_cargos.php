<?php namespace Aleksandrkrzhn\Portus\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAleksandrkrzhnPortusCargos extends Migration
{
    public function up()
    {
        Schema::create('aleksandrkrzhn_portus_cargos', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->text('description')->nullable();
            $table->unsignedInteger('order_id')->nullable();
            $table->double('weight', 10, 0)->nullable();
            $table->double('volume', 10, 0)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();

            $table
                ->foreign('order_id')
                ->references('id')
                ->on('aleksandrkrzhn_portus_orders')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('aleksandrkrzhn_portus_cargos');
    }
}
