<?php namespace Aleksandrkrzhn\Portus\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateAleksandrkrzhnPortusOrders extends Migration
{
    public function up()
    {
        Schema::create('aleksandrkrzhn_portus_orders', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('key', 6);
            $table->unsignedInteger('user_id')->nullable();
            $table->string('status')->default('Рассмотрение заявки');
            $table->string('address_from')->nullable();
            $table->string('address_to')->nullable();
            $table->unsignedInteger('total_cost')->nullable();
            $table->dateTime('delivery_date')->nullable();
            $table->text('stages')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->boolean('published')->default(false);

            $table->unique(['key']);
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::dropIfExists('aleksandrkrzhn_portus_orders');
    }
}
