<?php namespace Aleksandrkrzhn\Portus\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CustomizeUsersTable extends Migration
{

    public function up()
    {
        Schema::table('users', function($table) {
            $table->string('phone')->nullable();
        });
    }

    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('phone');
        });
    }

}
