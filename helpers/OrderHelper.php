<?php namespace Aleksandrkrzhn\Portus\Helpers;

use Aleksandrkrzhn\Portus\Models\Order;

class OrderHelper {
    public const ALLOWED_PREFIX_CHARS = ['A', 'B', 'C'];

    public static function generateKey(): string
    {
        $key = '';

        do {
            $numericPart = str_pad(random_int(0, 9999), 4, '0', STR_PAD_LEFT);
            $key = self::generateKeyPrefix() . $numericPart;
        } while (Order::where('key', '=', $key)->exists());

        return $key;
    }

    private static function generateKeyPrefix(): string
    {
        $prefix = '';
        $prefixLength = 2;
        $max = count(self::ALLOWED_PREFIX_CHARS) - 1;

        for ($i = 1; $i <= $prefixLength; $i++) {
            $prefix .= self::ALLOWED_PREFIX_CHARS[random_int(0, $max)];
        }

        return $prefix;
    }

    public static function isKeyPrefixPartValid($attribute, $value): bool
    {
        $prefix = substr($value, 0, 2);
        $isValid = true;

        foreach (str_split($prefix, 1) as $prefixChar) {
            if (! in_array($prefixChar, self::ALLOWED_PREFIX_CHARS)) {
                $isValid = false;
            }
        }

        return $isValid;
    }

    public static function isKeyNumericPartValid($attribute, $value): bool
    {
        $numericPart = substr($value, 2, 4);
        $isValid = true;

        foreach (str_split($numericPart, 1) as $num) {
            if (! is_numeric($num)) {
                $isValid = false;
            }
        }

        return $isValid;
    }
}
