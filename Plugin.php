<?php namespace Aleksandrkrzhn\Portus;

use Aleksandrkrzhn\Portus\Helpers\OrderHelper;
use Illuminate\Support\Facades\Log;
use System\Classes\PluginBase;
use Rainlab\User\Controllers\Users as UserController;
use RainLab\User\Models\User;
use Validator;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Aleksandrkrzhn\Portus\Components\ClientOrders'  => 'clientOrdersComponent',
        ];
    }

    public function registerSettings()
    {
    }

    public function boot()
    {
        // Custom rules for ID validation
        Validator::extend('order_key_prefix', function($attribute, $value, $parameters): bool {
            return OrderHelper::isKeyPrefixPartValid($attribute, $value);
        });

        Validator::extend('order_key_numeric_part', function($attribute, $value, $parameters): bool {
            return OrderHelper::isKeyNumericPartValid($attribute, $value);
        });

        User::extend(function($model) {
            // Get full name
            $model->addDynamicMethod('getFullnameAttribute', function() use ($model) {
                return $model->name . ' ' . $model->surname;
            });

            // Get orders
            $model->hasMany['orders'] = 'Aleksandrkrzhn\Portus\Models\Order';
        });

        UserController::extendFormFields(function($form, $model, $context) {
            if (!$model instanceof User) {
                return;
            }

            $form->addFields([
                'phone' => [
                    'label' => 'Номер телефона',
                    'type' => 'text',
                    'span' => 'auto',
                    'required' => true,
                ],
            ]);
        });
    }
}
