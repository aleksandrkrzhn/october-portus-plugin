<?php namespace Aleksandrkrzhn\Portus\Models;

use Model;
use \RainLab\User\Models\User;

/**
 * Model
 */
class Cargo extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'aleksandrkrzhn_portus_cargos';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required',
    ];

    public $belongsTo = [
        'order' => 'Aleksandrkrzhn\Portus\Models\Order',
    ];

    public $attachMany = [
        'cargo_images' => 'System\Models\File',
    ];

    protected $fillable = ['order_id', 'name', 'description', 'weight', 'volume'];

    public function getUserAttribute() : ?User {
        $order = $this->order;

        if (is_null($order)) {
            return null;
        }

        return $order->user;
    }
}
