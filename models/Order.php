<?php namespace Aleksandrkrzhn\Portus\Models;

use Model;
use Aleksandrkrzhn\Portus\Helpers\OrderHelper;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;

/**
 * Model
 */
class Order extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    const STATUS_PENDING = 'Рассмотрение заявки';
    const STATUS_IN_PROGRESS = 'В пути';
    const STATUS_DONE = 'Доставлено';

    protected $dates = ['deleted_at'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'aleksandrkrzhn_portus_orders';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'key' => [
            'bail',
            'required',
            'min:6',
            'max:6',
            'order_key_prefix',
            'order_key_numeric_part',
            'unique:aleksandrkrzhn_portus_orders',
        ],
    ];

    public $customMessages = [
        'order_key_prefix' => 'Префикс для KEY содержит запрещенные символы.',
        'order_key_numeric_part' => 'Последние четыре значения в KEY должны быть цифрами',
    ];

    public $belongsTo = [
        'user' => 'RainLab\User\Models\User',
    ];

    public $hasMany = [
        'cargo' => 'Aleksandrkrzhn\Portus\Models\Cargo',
    ];

    protected $fillable = ['user_id', 'address_from', 'address_to', 'key'];

    /**
     * @var array List of attribute names which are json encoded and decoded from the database.
     */
    protected $jsonable = ['stages'];

    public function getKeyAttribute(?string $key): string
    {
        if (is_null($key)) {
            $key = OrderHelper::generateKey();
        }
        return $key;
    }

    // "Proxy" for descriptionFrom setting for recordFinder in Cargo model
    public function getClientNameAttribute(): string
    {
        return $this->user->fullname;
    }

    // Manually validate user field
    // https://stackoverflow.com/a/48505843/12668009
    public function beforeValidate() {
        $userErrorMsg = 'Добавьте клиента!';

        // Если запрос с фронта
        if (! Request::is('backend/*')) {
            return;
        }

        // we need to check record is created or not
        if (is_null($this->id)) { // CREATE CASE
            // we need to use differ binding scope as this record is not saved yet.
            if ($this->user()->withDeferred(post('_session_key'))->count() === 0) {
                throw new \ValidationException(['user' => $userErrorMsg]);
            }
        } else { // UPDATE CASE
            // now record is created so we dont need differ binding
            if ($this->user()->count() === 0) {
                throw new \ValidationException(['user' => $userErrorMsg]);
            }
        }
    }

    public function hasPendingStatus(): bool {
        return $this->status === Order::STATUS_PENDING;
    }

    public function hasInProgressStatus(): bool {
        return $this->status === Order::STATUS_IN_PROGRESS;
    }

    public function hasDoneStatus(): bool {
        return $this->status === Order::STATUS_DONE;
    }
}
