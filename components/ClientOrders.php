<?php namespace Aleksandrkrzhn\Portus\Components;

use Aleksandrkrzhn\Portus\Models\Order;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Log;
use RainLab\User\Facades\Auth;
use RainLab\User\Models\User;

class ClientOrders extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Заказы клиента',
            'description' => 'Выводит список заказов авторизованного пользователя'
        ];
    }

    public function defineProperties()
    {
        return [
            'orders' => [
                'title'       => 'Количество',
                'description' => 'Определяет количество заказов на одной странице',
                'default'     => '10',
            ]
        ];
    }

    public function onRun()
    {
        // Get orders for current user
        $this->page['clientOrders'] = Order::where('user_id', '=', Auth::getUser()->id)->paginate($this->property('orders'));
    }
}
